package main

import (
	"context"
	"fmt"
	"log"
	"math/rand"
	"slices"
	"sync"
	"sync/atomic"
	"time"

	"github.com/georgysavva/scany/v2/pgxscan"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgxpool"
	"golang.org/x/exp/maps"
	"golang.org/x/sync/errgroup"
)

var schema string

var activeTasks = new(int64)

func init() {
	rand.Seed(time.Now().UnixNano())
	schema = fmt.Sprintf("schema_%d", rand.Int())
}

func main() {
	ctx := context.Background()
	db, err := openDB(ctx)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	if err := initialiseSchema(ctx, db); err != nil {
		log.Fatal(err)
	}

	if err := fillQueue(ctx, db, 10_000); err != nil {
		log.Fatal(err)
	}

	if err := launch(ctx, db, 100, 5); err != nil {
		log.Fatal(err)
	}
}

func fillQueue(ctx context.Context, db *pgxpool.Pool, n int) error {
	_, err := db.Exec(ctx, `INSERT INTO queue (id) SELECT gen_random_uuid() FROM generate_series(0, $1-1);`, n)
	if err != nil {
		return fmt.Errorf("insert: %w", err)
	}
	log.Printf("written %d rows", n)
	return nil
}

func launch(ctx context.Context, db *pgxpool.Pool, nWorkers, batchSize int) error {
	group, ctx := errgroup.WithContext(ctx)

	processed := map[uuid.UUID]int{}
	processedChan := make(chan uuid.UUID)
	processedMu := sync.Mutex{}
	processedMu.Lock()
	go func() {
		defer processedMu.Unlock()
		for id := range processedChan {
			processed[id]++
		}
	}()

	log.Printf("launching %d workers", nWorkers)
	for i := 0; i < nWorkers; i++ {
		ii := i
		group.Go(func() (err error) {
			done := false
			for err == nil && !done {
				done, err = doWork(ctx, ii, db, batchSize, processedChan)
			}
			return err
		})
	}

	go func() {
		for ctx.Err() == nil {
			log.Printf("active tasks: %d", atomic.LoadInt64(activeTasks))
			time.Sleep(1 * time.Second)
		}
	}()

	if err := group.Wait(); err != nil {
		return err
	}
	close(processedChan)
	processedMu.Lock()

	processedValues := maps.Values(processed)
	log.Println("min", slices.Min(processedValues), "max", slices.Max(processedValues))

	return nil
}

func doWork(ctx context.Context, label int, db *pgxpool.Pool, batchSize int, processedChan chan uuid.UUID) (bool, error) {
	ids, err := startJobs(ctx, db, batchSize)
	if err != nil {
		return false, fmt.Errorf("db: %w", err)
	}

	if len(ids) == 0 {
		log.Printf("[%d] done", label)
		return true, nil
	}

	atomic.AddInt64(activeTasks, int64(len(ids)))

	// Pretend to do some work on each job
	for _, id := range ids {
		time.Sleep(50 * time.Millisecond)
		processedChan <- id
	}

	if err := finishJobs(ctx, db, ids); err != nil {
		return false, fmt.Errorf("db: %w", err)
	}
	atomic.AddInt64(activeTasks, -int64(len(ids)))

	return false, nil
}

func startJobs(ctx context.Context, db *pgxpool.Pool, batchSize int) ([]uuid.UUID, error) {
	tx, err := db.Begin(ctx)
	if err != nil {
		return nil, fmt.Errorf("begin: %w", err)
	}
	defer tx.Rollback(ctx)

	// Pick some jobs from the queue, close the time range on their "queued" entries and return their IDs
	selectQueued := `
		WITH pending AS (
			SELECT id FROM queue_current WHERE status = 'queued' FOR UPDATE SKIP LOCKED LIMIT $1
		)
		UPDATE queue q
		SET valid_time = tstzrange(lower(q.valid_time), now(), '[)')
		FROM pending p
		WHERE q.id = p.id
		RETURNING p.id`

	var ids []uuid.UUID
	if err := pgxscan.Select(ctx, tx, &ids, selectQueued, batchSize); err != nil {
		return nil, fmt.Errorf("determine jobs to start: %w", err)
	}

	// Add new "started" entries for the jobs we've selected.
	if len(ids) > 0 {
		var startedRows [][]any
		for _, id := range ids {
			startedRows = append(startedRows, []any{id, "started"})
		}
		_, err = tx.CopyFrom(ctx, pgx.Identifier{"queue"}, []string{"id", "status"}, pgx.CopyFromRows(startedRows))
		if err != nil {
			return nil, fmt.Errorf("record jobs started: %w", err)
		}
	}

	if err := tx.Commit(ctx); err != nil {
		return nil, fmt.Errorf("commit: %w", err)
	}

	return ids, nil
}

func finishJobs(ctx context.Context, db *pgxpool.Pool, ids []uuid.UUID) error {
	tx, err := db.Begin(ctx)
	if err != nil {
		return fmt.Errorf("begin: %w", err)
	}
	defer tx.Rollback(ctx)

	// Close the time-range on the "started" entries of this batch of jobs
	finishSQL := `
		UPDATE queue q
		SET valid_time = tstzrange(lower(valid_time), now())
		WHERE q.id = ANY($1)
		AND q.status = 'started'`

	if _, err := tx.Exec(ctx, finishSQL, ids); err != nil {
		return fmt.Errorf("record no longer running: %w", err)
	}

	// Add new "finished" rows for the batch.
	var finishedRows [][]any
	for _, id := range ids {
		finishedRows = append(finishedRows, []any{id, "finished"})
	}
	_, err = tx.CopyFrom(ctx, pgx.Identifier{"queue"}, []string{"id", "status"}, pgx.CopyFromRows(finishedRows))
	if err != nil {
		return fmt.Errorf("record finished: %w", err)
	}

	if err := tx.Commit(ctx); err != nil {
		return fmt.Errorf("commit: %w", err)
	}

	return nil
}

func openDB(ctx context.Context) (*pgxpool.Pool, error) {
	config, err := pgxpool.ParseConfig("host=localhost port=5432 database=postgres user=postgres password=postgres")
	if err != nil {
		return nil, fmt.Errorf("parse config: %w", err)
	}
	config.ConnConfig.RuntimeParams = map[string]string{
		"search_path": schema,
	}
	config.MaxConns = 8
	return pgxpool.NewWithConfig(ctx, config)
}

func initialiseSchema(ctx context.Context, db *pgxpool.Pool) error {
	_, err := db.Exec(ctx, fmt.Sprintf("CREATE EXTENSION IF NOT EXISTS btree_gist SCHEMA public;"))
	if err != nil {
		return fmt.Errorf("create extension: %w", err)
	}

	_, err = db.Exec(ctx, fmt.Sprintf("CREATE SCHEMA %s;", schema))
	if err != nil {
		return fmt.Errorf("create schema: %w", err)
	}

	_, err = db.Exec(ctx, `
		CREATE TABLE queue (
			id          uuid,
			status      text DEFAULT 'queued',
			valid_time tstzrange DEFAULT tstzrange(now(), NULL, '[)'),
			PRIMARY KEY (id, status),
			EXCLUDE USING GIST (id WITH =, valid_time WITH &&)
		);
	`)
	if err != nil {
		return fmt.Errorf("create table: %w", err)
	}

	_, err = db.Exec(ctx, `
		CREATE INDEX queue_covering_idx ON queue(id, status) include (valid_time);
	`)
	if err != nil {
		return fmt.Errorf("create index: %w", err)
	}

	_, err = db.Exec(ctx, `
		CREATE VIEW queue_current AS
        SELECT id, status, lower(valid_time)
        FROM queue
		WHERE upper_inf(valid_time);
	`)
	if err != nil {
		return fmt.Errorf("create table: %w", err)
	}

	return nil
}
